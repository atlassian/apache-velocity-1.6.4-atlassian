## 1.6.4-atlassian-38

### Changed

- Visibility of SecureIntrospectorImpl#toMethodStr static method changed from package-private to public
- Added `equals` and `hashCode` to `ASTIdentifier` and `Info` classes so that the cache maps do not contain multiple instances for the same identifier

## 1.6.4-atlassian-37

### Added

- [DCA11Y-1177](https://hello.jira.atlassian.cloud/browse/DCA11Y-1177):: Added HTML traceability comments when rendering templates

### Changed

- The allowed file types which were being loaded from the configuration files in the `FileTypeAllowlistHelper`, weren't
  being converted into lowercase. This could lead to bypassing the check. It was fixed by converting all allowed
  file types into lowercase before verifying.

## 1.6.4-atlassian-36

### Added

- Introduce configuration option `introspector.validate.exemptions.disable` to opt-out of the breaking validation
  changes made to excluded package exempt class configuration in `1.6.4-atlassian-29`. This is useful for applications
  that need to exempt classes that cannot be loaded/validated on startup and validation cannot be deferred easily.

## 1.6.4-atlassian-35

### Added

- Introduce configuration option `introspector.validation.silent.enable` which will not log validation errors relating
  to exclusion or allow list configuration at warn level, instead logging at debug level

## 1.6.4-atlassian-34

### Changed

- Introduced new SecureIntrospectorImpl protected methods named *Cached, which unlike the methods named *Internal, can
  be overridden without the result being cached internally. This is useful for criteria which is dynamic and should not
  be cached such as those defined in plugin module descriptions which can be enabled/disabled at any time.
- Allow `velocimacro.library` to be set to `none` in defaults properties to disable loading of `VM_global_library.vm`

### Added

- Introduce configuration option `introspector.proper.allowlist.debug.enable` which when enabled alongside
  `introspector.proper.allowlist.enable` will log methods that would be blocked without actually blocking them. This is
  useful for collecting data on what methods are being called and can be used to inform the allowlist configuration.
- Add support for alternate default properties file `velocity-default.properties` as the naming of `override.properties`
  was causing confusion for product developers

## 1.6.4-atlassian-33

### Changed

- Velocity engines will now prefer `org/apache/velocity/runtime/defaults/override.properties` over
  `org/apache/velocity/runtime/defaults/velocity.properties` when loading default properties. The latter file won't be
  read at all when the former is present. This is useful in environments where the application is not in control of all
  Velocity engine instantiations and wishes to enforce its own default properties on such engines.

## 1.6.4-atlassian-32

### Changed

- Introduce `IntrospectorBase#getMethod(Object, Class, String, Object[])` to make Introspector instance aware
- Introduce `introspector.method.translator` extension point for the purpose of enforcing a consistent class allowlist
- Introduce overridable `SecureIntrospectorImpl#isIntrospectorEnabled` method to allow subclasses to defer class loading
- Introduce opt-in method/class/package allowlist, see the upgrade guide for more details

## 1.6.4-atlassian-31

### Breaking

- [DCA11Y-918] [DCA11Y-919] Put File and Filetype allowlists behind a configuration option 

## 1.6.4-atlassian-30

### Breaking
 
- [DCA11Y-918](https://hello.jira.atlassian.cloud/browse/DCA11Y-918) Introduce filetype allowlist, see the upgrade guide for more details
- [DCA11Y-919](https://hello.jira.atlassian.cloud/browse/DCA11Y-919) Introduce file allowlist, see the upgrade guide for more details

## 1.6.4-atlassian-29

### Breaking

- Excluded package exempt class configuration will now be validated by the Introspector on startup and any unloadable
  entries discarded

### Changed

- Bump commons-logging:commons-logging to 1.3.1
- Merged `com.atlassian.velocity.htmlsafe.introspection.AllowlistSecureIntrospector` from `velocity-htmlsafe` into its
  superclass in this repo (`SecureIntrospectorImpl`) to resolve fragmentation of code intended for similar use and to
  avoid undesirable interactions when making super calls.
- Updated primary constructor for `SecureIntrospectorImpl` for simpler subclassing

## 1.6.4-atlassian-25
- [CONFSERVER-78930](https://jira.atlassian.com/browse/CONFSERVER-78930) Velocity Template Injection in Custom User Macros
- Bump org.mockito.mockito-core from 3.10.0 to 3.12.
- Bump log4j from 1.2.12 to 1.2.17-atlassian-16
- [VULN-953684](https://asecurityteam.atlassian.net/browse/VULN-953684) Updated commons-text to 1.10.0

## 1.6.4-atlassian-24
- [BSP-3554](https://bulldog.internal.atlassian.com/browse/BSP-3554) add java.lang.reflect.Proxy to safe classes

## 1.6.4-atlassian-23
- [CONFSERVER-61047](https://jira.atlassian.com/browse/CONFSERVER-61047) Add allowlisted classes to `SecureIntrospectorImpl`

## 1.6.4-atlassian-22
- [VULN-266305](https://asecurityteam.atlassian.net/browse/VULN-266305) [SPFE-340](https://ecosystem.atlassian.net/browse/SPFE-340) - SECURITY - Bump apache commons collections to 3.2.2

## Earlier

- Bump apache commons lang3 from 3.8.1 to 3.12.0
- Bump apache commons text from 1.6 to 1.9
- Bump jdom from 1.0 to 1.1
- Bump apache commons logging from 1.1 to 1.2
- Bump ant from 1.6 to 1.7.0
- Bump apache commons pool from 1.5.4 to 1.6

## In the before CHANGELOG times

You may ask yourself; [where did all those versions go?](https://www.youtube.com/watch?v=jiks5ZfW0zQ) ¯\_(ツ)_/¯

Well they were probably burned. Here, early in the history it was `atlassian-1`: https://bitbucket.org/atlassian/apache-velocity-1.6.4-atlassian/src/22f537cfba83e675f032a32536f0a7dbd3366442/pom.xml 

Here's a list from merged PRs and commits on master

- [MNSTR-4500](https://bulldog.internal.atlassian.com/browse/MNSTR-4500) - FEATURE - Support reloading all velocity templates in dev.mode (when velocimacro.library.autoreload=true and .cache=false)
- [CONFSERVER-59422](https://jira.atlassian.com/browse/CONFSERVER-59422) - velocity introspector improvements
- [CONFSERVER-58523](https://jira.atlassian.com/browse/CONFSERVER-58523) Improving performance of Secure Introspector for big list of packages and classes
- [CONFSRVDEV-7520](https://jira.atlassian.com/browse/CONFSRVDEV-7520) - commons lang3
- [CONFDEV-32007](https://jira.atlassian.com/browse/CONFDEV-32007) - Don't fill stack trace for IOException used only for control flow
- [JDEV-29951](https://hello.atlassian.net/browse/JDEV-29951) support for java 1.5 iterable
- [JDEV-29161](https://hello.atlassian.net/browse/JDEV-29161) added SPI for IntrospectorCache
- [DEVSPEED-325](https://jdog.jira-dev.com/browse/DEVSPEED-325) Speed up discovery of getter calls on Maps by using the instance of operator.
- [CONF-19049](https://jira.atlassian.com/browse/CONFSERVER-19049) - refactor the Foreach.render method so it doesn't trigger a nasty memory-hogging JVM bug (merged from the 1.6.1-atlassian branch, commit originally by cmiller)
