# 1.6.4-atlassian-31 Upgrade guide

## For host products (e.g. Confluence)

### File and filetype allowlists

These are now disabled by default. Enable them by setting the properties `resource.loader.file.allowlist.enabled` and `resource.loader.filetype.allowlist.enabled`
