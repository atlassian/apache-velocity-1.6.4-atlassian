package org.apache.velocity.util.introspection;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang3.ArrayUtils;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.ClassUtils;
import org.apache.velocity.util.LazyRef;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.Collections.emptySet;
import static java.util.Collections.synchronizedMap;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.lang3.ClassUtils.getAllInterfaces;
import static org.apache.commons.lang3.ClassUtils.getAllSuperclasses;
import static org.apache.velocity.util.FilesystemUtils.containsPathTraversal;

/**
 * <p>Prevent "dangerous" classloader/reflection related calls.  Use this
 * introspector for situations in which template writers are numerous
 * or untrusted.  Specifically, this introspector prevents creation of
 * arbitrary objects and prevents reflection on objects.
 *
 * <p>See documentation of checkObjectExecutePermission() for
 * more information on specific classes and methods blocked.
 *
 * @author <a href="mailto:wglass@forio.com">Will Glass-Husain</a>
 * @since 1.5
 */
public class SecureIntrospectorImpl extends Introspector implements SecureIntrospectorControl {

    private final MethodTranslator methodTranslator;

    private final boolean isSilentValidation;
    private final Supplier<Set<String>> restrictedClassesRef;
    private final Set<String> restrictedPackages;
    private final Supplier<Set<Class<?>>> restrictedPackagesExemptClassesRef;
    private final Set<String> restrictedPackagesExemptClasses;
    private final Set<String> restrictedPackagesExemptPackages;
    private final boolean isAllowlistEnabled;
    private final boolean isAllowlistDebugMode;
    private final Supplier<Map<Class<?>, Set<String>>> allowlistedMethodsRef;
    private final Supplier<Set<Class<?>>> allowlistedClassesRef;
    private final Set<String> allowlistedPackages;
    private final Set<String> parameterFilteringExemptMethodNames;
    private final Map<Class<?>, Boolean> restrictedClassCache = synchronizedMap(new WeakHashMap<>());
    private final Map<Method, Boolean> allowlistedMethodCache = synchronizedMap(new WeakHashMap<>());
    private final Map<Class<?>, Boolean> allowlistedClassCache = synchronizedMap(new WeakHashMap<>());

    private SecureIntrospectorImpl(boolean isSilentValidation,
                                   String[] restrictedClasses,
                                   String[] restrictedPackages,
                                   String[] restrictedPackagesExemptClasses,
                                   boolean isExemptClassValidationDisabled,
                                   String[] restrictedPackagesExemptPackages,
                                   boolean isAllowlistEnabled,
                                   boolean isAllowlistDebugMode,
                                   String[] allowlistedMethods,
                                   String[] allowlistedClasses,
                                   String[] allowlistedPackages,
                                   String[] parameterFilteringExemptMethodNames,
                                   String methodTranslatorImpl,
                                   Log log,
                                   RuntimeServices runtimeServices) {
        super(log, runtimeServices);
        this.isSilentValidation = isSilentValidation;
        this.restrictedClassesRef = new LazyRef<>(() -> toValidatedClassSet(restrictedClasses));
        this.restrictedPackages = toParsedSet(restrictedPackages);
        if (isExemptClassValidationDisabled) {
            this.restrictedPackagesExemptClassesRef = Collections::emptySet;
            this.restrictedPackagesExemptClasses = unmodifiableSet(new HashSet<>(Arrays.asList(restrictedPackagesExemptClasses)));
        } else {
            this.restrictedPackagesExemptClassesRef = new LazyRef<>(() -> toClassSet(restrictedPackagesExemptClasses));
            this.restrictedPackagesExemptClasses = emptySet();
        }
        this.restrictedPackagesExemptPackages = toParsedSet(restrictedPackagesExemptPackages);
        this.isAllowlistEnabled = isAllowlistEnabled;
        this.isAllowlistDebugMode = isAllowlistDebugMode;
        this.parameterFilteringExemptMethodNames = toParsedSet(parameterFilteringExemptMethodNames);
        this.allowlistedMethodsRef = new LazyRef<>(() -> toMethodSet(allowlistedMethods));
        this.allowlistedClassesRef = new LazyRef<>(() -> toClassSet(allowlistedClasses));
        this.allowlistedPackages = toParsedSet(allowlistedPackages);
        this.methodTranslator = ClassUtils.getNewInstance(methodTranslatorImpl, MethodTranslator.class);
    }

    public SecureIntrospectorImpl(Log log, RuntimeServices runtimeServices) {
        this(
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATION_SILENT, false),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES),
                ArrayUtils.addAll(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES),
                        runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES)),
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATE_EXEMPTIONS_DISABLE, false),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES),
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false),
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_DEBUG, false),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS),
                runtimeServices.getConfiguration().getString(
                        RuntimeConstants.INTROSPECTOR_METHOD_TRANSLATOR, MethodTranslatorImpl.class.getName()),
                log,
                runtimeServices
        );
    }

    @Deprecated
    public SecureIntrospectorImpl(String[] restrictedClasses,
                                  String[] restrictedPackages,
                                  String[] restrictedPackagesExemptClasses,
                                  String[] parameterFilteringExemptMethodNames,
                                  Log log,
                                  RuntimeServices runtimeServices) {
        this(
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATION_SILENT, false),
                restrictedClasses,
                restrictedPackages,
                restrictedPackagesExemptClasses,
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATE_EXEMPTIONS_DISABLE, false),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES),
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false),
                runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_DEBUG, false),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES),
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES),
                parameterFilteringExemptMethodNames,
                runtimeServices.getConfiguration().getString(
                        RuntimeConstants.INTROSPECTOR_METHOD_TRANSLATOR, MethodTranslatorImpl.class.getName()),
                log,
                runtimeServices
        );
    }

    @Deprecated
    public SecureIntrospectorImpl(String[] restrictedClasses,
                                  String[] restrictedPackages,
                                  String[] restrictedPackagesExemptClasses,
                                  Log log,
                                  RuntimeServices runtimeServices) {
        this(
                restrictedClasses,
                restrictedPackages,
                restrictedPackagesExemptClasses,
                runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS),
                log,
                runtimeServices
        );
    }

    /**
     * Loading of classes (via {@link #loadClass}) required for enforcement of introspection will be deferred until
     * this method returns {@code true}. In OSGi applications, by overriding both these methods appropriately, it is
     * possible to load plugin/bundle classes into the Velocity exemption and allowlists.
     */
    protected boolean isIntrospectorEnabled() {
        return true;
    }

    /**
     * Exists for overriding flexibility
     */
    protected boolean isAllowlistEnabled() {
        return isAllowlistEnabled;
    }

    /**
     * Exists for overriding flexibility
     */
    protected boolean isAllowlistDebugMode() {
        return isAllowlistDebugMode;
    }

    public static Set<String> toParsedSet(String[] array) {
        return unmodifiableSet(Arrays.stream(array).map(String::trim).filter(s -> !s.isEmpty()).collect(toSet()));
    }

    @Override
    public Method getMethod(Class clazz, String methodName, Object[] params) throws IllegalArgumentException {
        return getMethod(null, clazz, methodName, params);
    }

    /**
     * Get the Method object corresponding to the given class, name and parameters.
     * Will check for appropriate execute permissions and return null if the method
     * is not allowed to be executed.
     *
     * @param clazz      Class on which method will be called
     * @param methodName Name of method to be called
     * @param params     array of parameters to method
     * @return Method object retrieved by Introspector
     * @throws IllegalArgumentException The parameter passed in were incorrect.
     */
    @Override
    public Method getMethod(Object target, Class clazz, String methodName, Object[] params) throws IllegalArgumentException {
        Method method = super.getMethod(clazz, methodName, params);
        if (method == null) {
            return null;
        }
        if (isExecutionRestricted(target, method, params)) {
            return null;
        }
        return method;
    }

    public boolean isExecutionRestricted(Object target, Method method, Object[] params) {
        if (isParametersRestricted(method, params)) {
            return true;
        }
        if (!checkObjectExecutePermission(target, method)) {
            return true;
        }
        return false;
    }

    protected boolean isParametersRestricted(Method method, Object[] params) {
        if (parameterFilteringExemptMethodNames.contains(method.getName())) {
            return false;
        }
        if (isParamsContainPathTraversal(params)) {
            log.warn(toPathTraversalWarning(method.getName(), method.getDeclaringClass(), params));
            return true;
        }
        return false;
    }

    /**
     * Checks if any string parameters could potentially lead to a path traversal attack.
     *
     * @param params the parameters to check
     * @return true if any unsafe paths were found, false otherwise
     */
    protected boolean isParamsContainPathTraversal(Object[] params) {
        return Arrays.stream(params).anyMatch(param -> param instanceof String && containsPathTraversal((String) param));
    }

    private static String toPathTraversalWarning(String methodName, Class clazz, Object[] params) {
        return format(
                "Found a potential path traversal attempt in the parameters: %s of method: %s from object of class: %s, rejecting due to security restrictions.",
                Arrays.toString(params),
                methodName,
                clazz.getName());
    }

    /**
     * Exists for backwards API compatibility. Not called in practice.
     */
    @Override
    public boolean checkObjectExecutePermission(Class clazz, String methodName) {
        if (!isIntrospectorEnabled()) {
            return true;
        }

        Boolean topLevelCheck = topLevelChecks(clazz, methodName);
        if (topLevelCheck != null) {
            return topLevelCheck;
        }
        Class<?> checkClass = resolveArrayClass(clazz);
        if (isRestrictedClass(checkClass)) {
            return false;
        }
        return isAllowlistedClass(checkClass);
    }

    /**
     * Default checks preserved from original Velocity code.
     */
    protected Boolean topLevelChecks(Class<?> clazz, String methodName) {
        if ("wait".equals(methodName) || "notify".equals(methodName)) {
            return false;
        } else if (Number.class.isAssignableFrom(clazz)) {
            return true;
        } else if (Boolean.class.isAssignableFrom(clazz)) {
            return true;
        } else if (String.class.isAssignableFrom(clazz)) {
            return true;
        } else if (Class.class.isAssignableFrom(clazz) && "getName".equals(methodName)) {
            return true;
        }
        return null;
    }

    /**
     * Target may be null when working with arrays or static methods
     * {@link UberspectImpl#getMethod(Object, String, Object[], Info)}.
     *
     * @since 1.6.4-atlassian-32
     */
    @Override
    public boolean checkObjectExecutePermission(Object target, Method method) {
        if (!isIntrospectorEnabled()) {
            return true;
        }

        if (method == null) {
            throw new IllegalArgumentException();
        }

        // Prefer checking target class if available
        Class<?> checkClass = target != null ? target.getClass() : method.getDeclaringClass();

        // Execute original top level checks
        Boolean topLevelCheck = topLevelChecks(checkClass, method.getName());
        if (topLevelCheck != null) {
            return topLevelCheck;
        }

        Method checkMethod = method;
        if (target != null) {
            checkMethod = methodTranslator.getTranslatedMethod(target, method);
            if (checkMethod != method) {
                // Prefer checking declaring class if target method was translated/unproxied
                checkClass = checkMethod.getDeclaringClass();
            }
        }
        checkClass = resolveArrayClass(checkClass);
        if (isRestrictedClass(checkClass)) {
            return false;
        }
        return isAllowlisted(checkMethod);
    }

    protected boolean isAllowlisted(Method method) {
        if (!isAllowlistEnabled()) {
            return true;
        }
        boolean result = isAllowlistedInternal(method);
        if (!result) {
            String msg = isAllowlistDebugMode() ?
                    "DEBUG MODE: Method needs allowlisting: " : "Invocation blocked as method is not allowlisted: ";
            log.warn(msg + method.getDeclaringClass().getName() + "#" + toMethodStr(method));
        }
        return result || isAllowlistDebugMode();
    }

    /**
     * Do not call directly, use {@link #isAllowlisted(Method)} instead.
     */
    protected boolean isAllowlistedInternal(Method method) {
        return isAllowlistedClassPackageCached(method.getDeclaringClass()) || isAllowlistedMethodCached(method);
    }

    /**
     * Exists for backwards API compatibility. Not called in practice.
     */
    protected boolean isAllowlistedClass(Class<?> clazz) {
        if (!isAllowlistEnabled()) {
            return true;
        }
        boolean result = isAllowlistedClassPackageCached(clazz);
        if (!result) {
            String msg = isAllowlistDebugMode() ?
                    "DEBUG MODE: Class needs allowlisting: " : "Invocation blocked as class is not allowlisted: ";
            log.warn(msg + clazz.getName());
        }
        return result || isAllowlistDebugMode();
    }

    protected Class<?> resolveArrayClass(Class<?> clazz) {
        return clazz.isArray() ? clazz.getComponentType() : clazz;
    }

    protected boolean isRestrictedClass(Class<?> clazz) {
        boolean result = isRestrictedClassPackageCached(clazz);
        if (result) {
            log.warn("Invocation blocked as class is restricted: " + clazz.getName());
        }
        return result;
    }

    /**
     * Do not call directly, use {@link #isRestrictedClass(Class)} instead. Can be overridden by subclasses to introduce
     * additional restriction criteria that should NOT be cached.
     */
    protected boolean isRestrictedClassPackageCached(Class<?> clazz) {
        return restrictedClassCache.computeIfAbsent(clazz, this::isRestrictedClassPackageInternal);
    }

    /**
     * Do not call directly, use {@link #isRestrictedClass(Class)} instead. Can be overridden by subclasses to introduce
     * additional restriction criteria that should be cached.
     */
    protected boolean isRestrictedClassPackageInternal(Class<?> clazz) {
        Set<Class<?>> allCheckClasses = getAllInterfacesAndSuperclasses(clazz);
        return allCheckClasses.stream().anyMatch(this::isClassRestricted) ||
                allCheckClasses.stream().anyMatch(this::isClassPackageRestricted);
    }

    /**
     * Do not call directly, use {@link #isAllowlisted(Method)} instead. Can be overridden by subclasses to introduce
     * additional allowlist criteria that should NOT be cached.
     */
    protected boolean isAllowlistedClassPackageCached(Class<?> clazz) {
        return allowlistedClassCache.computeIfAbsent(clazz, this::isAllowlistedClassPackageInternal);
    }

    /**
     * Do not call directly, use {@link #isAllowlisted(Method)} instead. Can be overridden by subclasses to introduce
     * additional allowlist criteria that should be cached.
     */
    protected boolean isAllowlistedClassPackageInternal(Class<?> clazz) {
        return clazz.isPrimitive() || allowlistedClassesRef.get().contains(clazz) ||
                isPackageMatches(clazz.getPackage(), allowlistedPackages);
    }

    /**
     * Do not call directly, use {@link #isAllowlisted(Method)} instead. Can be overridden by subclasses to introduce
     * additional allowlist criteria that should NOT be cached.
     */
    protected boolean isAllowlistedMethodCached(Method method) {
        return allowlistedMethodCache.computeIfAbsent(method, this::isAllowlistedMethodInternal);
    }

    /**
     * Do not call directly, use {@link #isAllowlisted(Method)} instead. Can be overridden by subclasses to introduce
     * additional allowlist criteria that should be cached.
     */
    protected boolean isAllowlistedMethodInternal(Method method) {
        // Allowlist is consulted based on the declaring class of a method, target class is NOT considered
        // This was an intentional design decision to simplify allowlist configuration
        Set<String> methodAllowlist = allowlistedMethodsRef.get().get(method.getDeclaringClass());
        return methodAllowlist != null && methodAllowlist.contains(toMethodStr(method));
    }

    /**
     * {@code introspector.proper.allowlist.methods} comma-delimited value format
     */
    public static String toMethodStr(Method method) {
        return method.getName() + "(" + Arrays.stream(method.getParameterTypes()).map(Class::getName).collect(joining(" ")) + ")";
    }

    protected Set<String> toValidatedClassSet(String[] classNames) {
        return unmodifiableSet(toClassSet(classNames).stream().map(Class::getName).collect(toSet()));
    }

    protected Set<Class<?>> toClassSet(String[] classNames) {
        Set<Class<?>> classSet = new HashSet<>();
        for (String className : toParsedSet(classNames)) {
            try {
                classSet.add(loadClass(className));
            } catch (ClassNotFoundException e) {
                logValidationError("Cannot load class for security introspection in Velocity: " + className);
            }
        }
        return unmodifiableSet(classSet);
    }

    protected Map<Class<?>, Set<String>> toMethodSet(String[] methodList) {
        Map<String, Set<String>> parsedMethods = toParsedMethodMap(Arrays.asList(methodList));
        Map<Class<?>, Set<String>> loadedMethods = new HashMap<>();
        for (Map.Entry<String, Set<String>> entry : parsedMethods.entrySet()) {
            String clazzName = entry.getKey();
            Set<String> methodNames = entry.getValue();
            Class<?> clazz;
            try {
                clazz = loadClass(clazzName);
            } catch (ClassNotFoundException e) {
                logValidationError("Cannot load class for security introspection in Velocity: " + clazzName);
                continue;
            }
            Set<String> invalidMethods = findUndeclaredMethods(clazz, methodNames);
            if (!invalidMethods.isEmpty()) {
                logValidationError(format("Cannot locate method(s) %s for class %s for security introspection in Velocity",
                        join(", ", invalidMethods), clazz.getName()));
                methodNames.removeAll(invalidMethods);
            }
            loadedMethods.put(clazz, methodNames);
        }
        return unmodifiableMap(loadedMethods);
    }

    private Map<String, Set<String>> toParsedMethodMap(Collection<String> methodList) {
        Map<String, Set<String>> methods = new HashMap<>();
        for (String methodStr : methodList) {
            String[] parts = methodStr.trim().split("#");
            if (parts.length != 2) {
                logValidationError("Invalid method format, should be \"package.Class#method(qualified.paramType1 qualified.paramType2)\", provided method was: " + methodStr);
            }
            String className = parts[0];
            String methodName = parts[1];
            methods.putIfAbsent(className, new HashSet<>());
            methods.get(className).add(methodName);
        }
        return unmodifiableMap(methods);
    }

    private static Set<String> findUndeclaredMethods(Class<?> clazz, Set<String> methods) {
        Set<String> declaredMethods = Arrays.stream(clazz.getDeclaredMethods()).map(SecureIntrospectorImpl::toMethodStr).collect(toSet());
        return methods.stream().filter(method -> !declaredMethods.contains(method)).collect(toSet());
    }

    /**
     * Exists for overriding purposes.
     */
    protected Class<?> loadClass(String name) throws ClassNotFoundException {
        return ClassUtils.getClass(name);
    }

    /**
     * Do not call directly, use {@link #isRestrictedClass(Class)} instead.
     */
    protected boolean isClassRestricted(Class<?> clazz) {
        return restrictedClassesRef.get().contains(clazz.getName());
    }

    /**
     * Do not call directly, use {@link #isRestrictedClass(Class)} instead.
     */
    protected boolean isClassPackageRestricted(Class<?> clazz) {
        return !restrictedPackagesExemptClassesRef.get().contains(clazz) &&
                !restrictedPackagesExemptClasses.contains(clazz.getName()) &&
                !restrictedPackagesExemptPackages.contains(toPackageName(clazz.getPackage())) &&
                isPackageMatches(clazz.getPackage(), restrictedPackages);
    }

    public static String toPackageName(Package packag) {
        return packag != null ? packag.getName() : "";
    }

    /**
     * Checks if the given package is equivalent to or a sub-package of the given matchingPackages.
     */
    public static boolean isPackageMatches(Package packag, Set<String> matchingPackages) {
        List<String> packageParts = Arrays.asList(toPackageName(packag).split("\\."));
        return IntStream.range(0, packageParts.size())
                .mapToObj(i -> String.join(".", packageParts.subList(0, i + 1)))
                .anyMatch(matchingPackages::contains);
    }

    private Set<Class<?>> getAllInterfacesAndSuperclasses(Class<?> clazz) {
        Set<Class<?>> allInterfacesAndClass = new HashSet<>();
        allInterfacesAndClass.add(clazz);
        allInterfacesAndClass.addAll(getAllInterfaces(clazz));
        allInterfacesAndClass.addAll(getAllSuperclasses(clazz));
        return allInterfacesAndClass;
    }

    private void logValidationError(String message) {
        if (!isSilentValidation) {
            log.warn(message);
        } else {
            log.debug(message);
        }
    }
}
