package org.apache.velocity.util.introspection;

import java.lang.reflect.Method;

/**
 * When {@link SecureIntrospectorImpl} encounters a {@link Method} on a proxied object, it will attempt to translate
 * the method to the corresponding method on the underlying object for the purpose of introspection.
 *
 * @since 1.6.4-atlassian-32
 */
public interface MethodTranslator {

    Method getTranslatedMethod(Object target, Method method);
}
