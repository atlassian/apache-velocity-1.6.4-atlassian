package org.apache.velocity.util.introspection;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.WeakHashMap;

import static java.util.Collections.synchronizedMap;

/**
 * Default {@link MethodTranslator} implementation which simply translates proxied methods to their originating
 * interface. It is recommended to use a more accurate custom implementation so that the allowlist is more secure.
 *
 * @since 1.6.4-atlassian-32
 */
public class MethodTranslatorImpl implements MethodTranslator {

    private static final Map<Method, Method> interfaceMethodCache = synchronizedMap(new WeakHashMap<>());

    @Override
    public Method getTranslatedMethod(Object target, Method method) {
        if (!target.getClass().getSimpleName().contains("$")) {
            return method;
        }
        return getInterfaceMethodIfPossible(method, target.getClass());
    }

    /**
     * Copied from org.springframework.util.ClassUtils.
     */
    public static Method getInterfaceMethodIfPossible(Method method, Class<?> targetClass) {
        if (!Modifier.isPublic(method.getModifiers()) || method.getDeclaringClass().isInterface()) {
            return method;
        }
        Method result = interfaceMethodCache.computeIfAbsent(method,
                key -> findInterfaceMethodIfPossible(key, key.getDeclaringClass(), Object.class));
        if (result == method && targetClass != null) {
            result = findInterfaceMethodIfPossible(method, targetClass, method.getDeclaringClass());
        }
        return result;
    }

    private static Method findInterfaceMethodIfPossible(Method method, Class<?> startClass, Class<?> endClass) {
        Class<?> current = startClass;
        while (current != null && current != endClass) {
            Class<?>[] ifcs = current.getInterfaces();
            for (Class<?> ifc : ifcs) {
                try {
                    return ifc.getMethod(method.getName(), method.getParameterTypes());
                } catch (NoSuchMethodException ex) {
                    // ignore
                }
            }
            current = current.getSuperclass();
        }
        return method;
    }
}
