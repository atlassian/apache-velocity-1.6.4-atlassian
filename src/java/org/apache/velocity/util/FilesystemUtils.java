package org.apache.velocity.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @since 8.6, copied from com.atlassian.confluence.util.FilesystemUtils
 */
public final class FilesystemUtils {
    public static final List<String> FORBIDDEN_PATH_EQUALS = Collections.singletonList("..");
    public static final List<String> FORBIDDEN_PATH_CONTAINS = Arrays.asList("../", "..\\");
    public static final List<String> FORBIDDEN_PATH_ENDINGS = Arrays.asList("/..", "\\..");

    /**
     * Checks if the given string contains any of the forbidden path traversal patterns.
     *
     * @param str The string to check
     * @return True if the string contains any of the forbidden path traversal patterns
     */
    public static boolean containsPathTraversal(String str) {
        return (FORBIDDEN_PATH_EQUALS.stream().anyMatch(str::equals)
                || FORBIDDEN_PATH_CONTAINS.stream().anyMatch(str::contains)
                || FORBIDDEN_PATH_ENDINGS.stream().anyMatch(str::endsWith));
    }

    /**
     * Ensures the path is safe and is not vulnerable to path traversal vulnerabilities.
     * On top of filesystem specific checks it also rejects any paths containing risky
     * path elements known to cause path traversal issues in different file systems (e.g. `..`, `/..`, `..\`).
     *
     * @param path The path to check
     * @return True if the path is safe
     */
    public static boolean isSafePath(String path) {
        if (Paths.get(path).isAbsolute()) {
            return false;
        }
        Path validatePath = Paths.get("root", path);
        if (validatePath.normalize() != validatePath) {
            return false;
        }
        return !containsPathTraversal(path);
    }

    /**
     * @param path The path to check
     * @return True if the path is safe
     * @see #isSafePath(String)
     */
    public static boolean isSafePath(Path path) {
        if (path.isAbsolute()) {
            return false;
        }
        return isSafePath(path.toString());
    }
}
