package org.apache.velocity.util;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Arrays;


/**
 * Simple utility functions for manipulating classes and resources from the classloader.
 *
 * @author <a href="mailto:wglass@apache.org">Will Glass-Husain</a>
 * @since 1.5
 */
public class ClassUtils
{

    /**
     * Utility class; cannot be instantiated.
     */
    private ClassUtils()
    {
    }

    /**
     * Return the specified class.  Checks the ThreadContext classloader first, then uses the System classloader. Should
     * replace all calls to <code>Class.forName( claz )</code> (which only calls the System class loader) when the class
     * might be in a different classloader (e.g. in a webapp).
     *
     * @param clazz the name of the class to instantiate
     * @return the requested Class object
     */
    public static Class getClass(String clazz) throws ClassNotFoundException
    {
        /**
         * Use the Thread context classloader if possible
         */
        final ClassLoader loader = Thread.currentThread().getContextClassLoader();
        if (loader != null)
        {
            try
            {
                return Class.forName(clazz, true, loader);
            }
            catch (final ClassNotFoundException E)
            {
                /**
                 * If not found with ThreadContext loader, fall thru to
                 * try System classloader below (works around bug in ant).
                 */
            }
        }
        /**
         * Thread context classloader isn't working out, so use system loader.
         */
        return Class.forName(clazz);
    }

    /**
     * Return a new instance of the given class.  Checks the ThreadContext classloader first, then uses the System
     * classloader.  Should replace all calls to <code>Class.forName( claz ).newInstance()</code> (which only calls the
     * System class loader) when the class might be in a different classloader (e.g. in a webapp).
     *
     * @param clazz the name of the class to instantiate
     * @return an instance of the specified class
     */
    public static Object getNewInstance(String clazz)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException
    {
        return getClass(clazz).newInstance();
    }

    /**
     * Finds a resource with the given name.  Checks the Thread Context classloader, then uses the System classloader.
     * Should replace all calls to {@link ClassLoader#getResource(String)} when the resource might come from a different
     * classloader.  (e.g. a webapp).
     *
     * @param claz Class to use when getting the System classloader (used if no Thread Context classloader available or
     * fails to get resource).
     * @param name name of the resource
     * @return URL for the resource.
     */
    public static URL getResource(Class claz, String name)
    {
        URL result;

        /**
         * remove leading slash so path will work with classes in a JAR file
         */
        while (name.startsWith("/"))
        {
            name = name.substring(1);
        }

        ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();

        if (classLoader == null)
        {
            classLoader = claz.getClassLoader();
            result = classLoader.getResource(name);
        }
        else
        {
            result = classLoader.getResource(name);

            /**
             * for compatibility with texen / ant tasks, fall back to
             * old method when resource is not found.
             */

            if (result == null)
            {
                classLoader = claz.getClassLoader();
                if (classLoader != null)
                { result = classLoader.getResource(name); }
            }
        }

        return result;

    }

    /**
     * Finds a resource with the given name.  Checks the Thread Context classloader, then uses the System classloader.
     * Should replace all calls to <code>Class.getResourceAsString</code> when the resource might come from a different
     * classloader.  (e.g. a webapp).
     *
     * @param claz Class to use when getting the System classloader (used if no Thread Context classloader available or
     * fails to get resource).
     * @param name name of the resource
     * @return InputStream for the resource.
     */
    public static InputStream getResourceAsStream(Class claz, String name)
    {
        InputStream result;

        /**
         * remove leading slash so path will work with classes in a JAR file
         */
        while (name.startsWith("/"))
        {
            name = name.substring(1);
        }

        ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();

        if (classLoader == null)
        {
            classLoader = claz.getClassLoader();
            result = classLoader.getResourceAsStream(name);
        }
        else
        {
            result = classLoader.getResourceAsStream(name);

            /**
             * for compatibility with texen / ant tasks, fall back to
             * old method when resource is not found.
             */

            if (result == null)
            {
                classLoader = claz.getClassLoader();
                if (classLoader != null)
                { result = classLoader.getResourceAsStream(name); }
            }
        }

        return result;

    }

    public static <T> T getNewInstance(final String className, Class<T> expectedInterface, final Object... params)
            throws ClassConstructionException
    {
        if (StringUtils.isNotEmpty(className))
        {
            final Class<T> clazz;
            try
            {
                //noinspection unchecked
                clazz = org.apache.velocity.util.ClassUtils.getClass(className);
            }
            catch (final ClassNotFoundException cnfe)
            {
                throw new ClassResolutionException("The specified class for (" + className + ") does not exist or is not accessible to the current classloader.", cnfe);
            }
            catch (final Exception e)
            {
                throw new ClassResolutionException("The specified class (" + className + ") can not be loaded.");
            }
            if (!expectedInterface.isAssignableFrom(clazz))
            {
                throw new ClassResolutionException("The specified class (" + className + ") does not implement " + expectedInterface.getName() + ".");
            }
            try
            {
                for (Constructor<?> constructor : clazz.getConstructors()) {
                    if (isCompatibleConstructor(constructor, params)) {
                        return (T) constructor.newInstance(params);
                    }
                }
                throw new NoSuchMethodException("No constructor found for " + className + " with the specified parameters " + Arrays.toString(params));
            }
            catch (final InstantiationException e)
            {
                throw new ClassConstructionException(e);
            }
            catch (final IllegalAccessException e)
            {
                throw new ClassConstructionException(e);
            }
            catch (final InvocationTargetException e)
            {
                throw new ClassConstructionException(e);
            }
            catch (final NoSuchMethodException e)
            {
                throw new ClassConstructionException(e);
            }
        }
        else
        {
            throw new ClassConstructionException("No class name specified");
        }
    }

    private static boolean isCompatibleConstructor(Constructor<?> constructor, Object[] params) {
        Class<?>[] paramTypes = constructor.getParameterTypes();
        if (paramTypes.length != params.length) {
            return false;
        }
        for (int i = 0; i < paramTypes.length; i++) {
            if (!paramTypes[i].isAssignableFrom(params[i].getClass())) {
                return false;
            }
        }
        return true;
    }
}
