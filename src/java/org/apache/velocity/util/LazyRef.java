package org.apache.velocity.util;

import java.util.function.Supplier;

/**
 * @since 1.6.4-atlassian-32
 */
public class LazyRef<T> implements Supplier<T> {

    private volatile T data;
    protected final Supplier<T> supplier;

    public LazyRef(Supplier<T> supplier) {
        this.supplier = supplier;
    }

    @Override
    public T get() {
        if (data == null) {
            synchronized (this) {
                if (data == null) {
                    data = supplier.get();
                }
            }
        }
        return data;
    }
}
