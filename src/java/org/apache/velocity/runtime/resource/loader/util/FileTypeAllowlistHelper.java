package org.apache.velocity.runtime.resource.loader.util;

import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.util.ConfigUtil;

import java.util.List;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public class FileTypeAllowlistHelper {
    private final List<String> lowercaseTrimmedAllowedFileTypes;
    private final boolean enabled;

    public FileTypeAllowlistHelper(final RuntimeServices runtimeServices, final String allowedFileTypesPropertyName) {
        requireNonNull(runtimeServices, "runtimeServices");
        requireNonNull(allowedFileTypesPropertyName, "allowedFileTypesPropertyName");

        lowercaseTrimmedAllowedFileTypes = ConfigUtil.getStrings(runtimeServices, allowedFileTypesPropertyName)
                .stream()
                .filter(extension -> {
                    if (!extension.startsWith(".")) {
                        throw new RuntimeException("Allowed Velocity file extension must start with '.' instead" +
                                " got: " + extension);
                    }

                    return true;
                })
                .map(String::toLowerCase)
                .collect(toList());

        enabled = runtimeServices.getBoolean(RuntimeConstants.RESOURCE_FILETYPE_ALLOWLIST_ENABLE, false);
    }

    public boolean isValidFileType(String templateName) {
        requireNonNull(templateName, "templateName");

        if (!enabled) {
            return true;
        }

        // we don't care about the casing, we might as well take the DX - we only care about "features" that allow
        // for uploading entirely different extensions
        templateName = templateName.toLowerCase();

        // perf note - we expect this set to be small with small strings
        return lowercaseTrimmedAllowedFileTypes.stream().anyMatch(templateName::endsWith);
    }
}
