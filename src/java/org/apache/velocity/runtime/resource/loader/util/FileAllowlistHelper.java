package org.apache.velocity.runtime.resource.loader.util;

import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.util.ConfigUtil;

import java.util.Objects;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;

public class FileAllowlistHelper {
    private static final Pattern LEADING_SLASHES = Pattern.compile("^/+");

    private final Set<String> allowedFilenames;
    private final boolean enabled;

    public FileAllowlistHelper(final RuntimeServices runtimeServices) {
        requireNonNull(runtimeServices, "runtimeServices");

        allowedFilenames = ConfigUtil.getStrings(runtimeServices, RuntimeConstants.RESOURCE_FILE_ALLOWLIST)
                .stream()
                .map(FileAllowlistHelper::stripLeadingForwardSlashes)
                .filter(Objects::nonNull)
                .filter(filename -> !filename.isEmpty())
                .collect(toSet());

        enabled = runtimeServices.getBoolean(RuntimeConstants.RESOURCE_FILE_ALLOWLIST_ENABLE, false);
    }

    public boolean isAllowed(final String templateName) {
        requireNonNull(templateName, "templateName");

        if (!enabled) {
            return true;
        }

        // perf note - we expect this set to be upto a thousand with medium length strings
        final String strippedTemplateName = stripLeadingForwardSlashes(templateName);
        return allowedFilenames.contains(strippedTemplateName);
    }

    private static String stripLeadingForwardSlashes(String string) {
        if (isNull(string)) {
            return null;
        }

        return LEADING_SLASHES.matcher(string).replaceAll("");
    }
}
