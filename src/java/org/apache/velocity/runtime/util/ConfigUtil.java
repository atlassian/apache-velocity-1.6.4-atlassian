package org.apache.velocity.runtime.util;

import org.apache.velocity.runtime.RuntimeServices;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

public final class ConfigUtil {
    public static List<String> getStrings(final RuntimeServices runtimeServices, final String propertyName) {
        requireNonNull(runtimeServices, "runtimeServices");
        requireNonNull(propertyName, "propertyName");

        final Object rawValue = runtimeServices.getProperty(propertyName);

        if (isNull(rawValue)) {
            return emptyList();
        } else if (rawValue instanceof String) {
            return singletonList(((String) rawValue).trim());
        } else if (rawValue instanceof List) {
            return ((List<?>) rawValue)
                    .stream()
                    .map(Object::toString) // casting gymnastics - we already know it should be a String
                    .map(String::trim)
                    .collect(toList());
        }

        throw new RuntimeException("Velocity property was not a string nor a list: " + propertyName);
    }
}
