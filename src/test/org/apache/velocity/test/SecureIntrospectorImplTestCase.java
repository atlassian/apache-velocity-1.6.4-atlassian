package org.apache.velocity.test;

import org.apache.velocity.runtime.RuntimeInstance;
import org.apache.velocity.util.introspection.Info;
import org.apache.velocity.util.introspection.Uberspect;
import org.apache.velocity.util.introspection.VelMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

public class SecureIntrospectorImplTestCase extends BaseTestCase {
    private Uberspect uberspect;

    public SecureIntrospectorImplTestCase(String name) {
        super(name);
    }

    public void setUp()
            throws Exception {
        init(emptyMap());
    }

    private void init(Map<String, String> props) throws Exception {
        RuntimeInstance runtimeInstance = new RuntimeInstance();
        runtimeInstance.setProperty(
                "runtime.introspector.uberspect", "org.apache.velocity.util.introspection.SecureUberspector");
        props.forEach(runtimeInstance::setProperty);
        runtimeInstance.init();
        uberspect = runtimeInstance.getUberspect();
    }

    public void testGetMethodNullForPathTraversal()
            throws Exception {
        Object[] params = {"../../../test.vm"};
        VelMethod method = uberspect.getMethod(String.class, "getName", params, null);
        assertNull(method);
    }

    public void testGetMethodNotNullForPathTraversal()
            throws Exception {
        Object[] params = {"test.vm"};
        VelMethod method = uberspect.getMethod(String.class, "getName", params, null);
        assertNotNull(method);
    }

    public void testGetMethodNotNullForPathTraversalWithAllowListedMethod()
            throws Exception {
        // (Allowlisted in test resources velocity.properties)
        Object[] params = {"../../../test.vm"};
        VelMethod method = uberspect.getMethod(MethodProvider.class, "htmlEncode", params, null);
        assertNotNull(method);
    }

    public void testGetMethodNullForPathTraversalWithNoLongerAllowListedMethod()
            throws Exception {
        init(singletonMap("introspector.allowlist.methods", ""));

        Object[] params = {"../../../test.vm"};
        VelMethod method = uberspect.getMethod(MethodProvider.class, "htmlEncode", params, null);
        assertNull(method);
    }

    public void testIteratorNeedsAllowlisting()
            throws Exception {
        init(singletonMap("introspector.proper.allowlist.enable", "true"));
        Iterable<?> iterable = new ArrayList<>();
        Info info = new Info("", 0, 0);

        assertNull(uberspect.getIterator(iterable, info));

        Map<String, String> props = new HashMap<>();
        props.put("introspector.proper.allowlist.enable", "true");
        props.put("introspector.proper.allowlist.methods",
                "java.util.ArrayList#iterator(), java.util.ArrayList$Itr#hasNext(), java.util.ArrayList$Itr#next()");
        init(props);

        assertNotNull(uberspect.getIterator(iterable, info));
    }

    public static class MethodProvider {
        public String htmlEncode(String param) {
            return param;
        }
    }
}
