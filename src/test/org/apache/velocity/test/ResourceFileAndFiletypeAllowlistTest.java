package org.apache.velocity.test;

import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.requireNonNull;
import static org.apache.velocity.runtime.RuntimeConstants.CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.FILE_RESOURCE_FILETYPE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.FILE_RESOURCE_LOADER_PATH;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILETYPE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILE_ALLOWLIST;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_FILE_ALLOWLIST_ENABLE;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_LOADER;
import static org.apache.velocity.runtime.RuntimeConstants.RESOURCE_TRUSTED_PROTOCOLS;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ResourceFileAndFiletypeAllowlistTest {
    private static final String SIMPLE_VM = "simple.vm";
    private static final String TEST_TXT = "test.txt";
    private static final String VM_GLOBAL_LIBRARY_VM = "VM_global_library.vm";

    // ----------------- FILE RESOURCE LOADER -----------------

    @Test
    void testFileResourceLoader_shouldAllowAllFilesAndFiletypesByDefault() {
        // given
        VelocityEngine velocityEngine = newEngineWithFileRL();

        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE), TRUE);
        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILE_ALLOWLIST_ENABLE), TRUE);

        // then should be no problem
        assertTrue(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    void testFileResourceLoader_shouldBlockRandomFiletypesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithFileRL();

        // when
        velocityEngine.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // then
        assertFalse(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    void testFileResourceLoaderFileTypeAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithFileRL();

        Velocity.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(FILE_RESOURCE_FILETYPE_ALLOWLIST, ".txt");

        // then
        assertTrue(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    void testFileResourceLoader_shouldBlockRandomFilesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithFileRL();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, VM_GLOBAL_LIBRARY_VM);

        // then
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    void testFileResourceLoaderFileAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithFileRL();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, "VM_global_library.vm,simple.vm");

        // then
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    private static VelocityEngine newEngineWithFileRL() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RESOURCE_LOADER, "file");
        velocityEngine.setProperty(FILE_RESOURCE_LOADER_PATH, "test/templates");
        return velocityEngine;
    }

    // ----------------- CLASSPATH RESOURCE LOADER -----------------

    @Test
    void testClasspathResourceLoader_shouldAllowAllFilesAndFiletypesByDefault() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE), TRUE);
        assertNotEquals(velocityEngine.getProperty(RESOURCE_FILE_ALLOWLIST_ENABLE), TRUE);

        // then should be no problem
        assertTrue(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    void testClasspathResourceLoader_shouldBlockRandomFiletypesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        // when
        velocityEngine.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // then
        assertFalse(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    void testClasspathResourceLoaderFileTypeAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        Velocity.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(CLASSPATH_RESOURCE_FILETYPE_ALLOWLIST, ".txt");

        // then
        assertTrue(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    void testClasspathResourceLoader_shouldBlockRandomFilesOOTB() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, VM_GLOBAL_LIBRARY_VM);

        // then
        assertFalse(velocityEngine.resourceExists(TEST_TXT));
    }

    @Test
    void testClassResourceLoaderFileAllowlist_shouldBeConfigurable() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");

        // when
        Velocity.setProperty(RESOURCE_FILE_ALLOWLIST, "VM_global_library.vm,simple.vm");

        // then
        assertFalse(velocityEngine.resourceExists(SIMPLE_VM));
    }

    @Test
    void testClassResourceLoaderFileAndFiletypeAllowlist_shouldRespectTrustedProtocols() {
        // given
        VelocityEngine velocityEngine = newEngineWithClasspathRL();

        velocityEngine.setProperty(RESOURCE_FILE_ALLOWLIST_ENABLE, "true");
        velocityEngine.setProperty(RESOURCE_FILETYPE_ALLOWLIST_ENABLE, "true");

        // when
        velocityEngine.setProperty(RESOURCE_TRUSTED_PROTOCOLS, "file");

        // then
        assertTrue(velocityEngine.resourceExists(TEST_TXT));
    }

    private VelocityEngine newEngineWithClasspathRL() {
        VelocityEngine velocityEngine = new VelocityEngine();

        velocityEngine.setProperty(RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getCanonicalName());

        try {
            try (FileWriter writer = newClasspathFileWriter(SIMPLE_VM)) {
                writer.write("hi");
            }
            try (FileWriter writer = newClasspathFileWriter(TEST_TXT)) {
                writer.write("hi");
            }
            try (FileWriter writer = newClasspathFileWriter(VM_GLOBAL_LIBRARY_VM)) {
                writer.write("hi");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return velocityEngine;
    }

    private FileWriter newClasspathFileWriter(String filename) throws IOException {
        String basePath = requireNonNull(getClass().getClassLoader().getResource("")).getPath();
        String absoluteFilename = basePath + filename;
        return new FileWriter(absoluteFilename);
    }
}
