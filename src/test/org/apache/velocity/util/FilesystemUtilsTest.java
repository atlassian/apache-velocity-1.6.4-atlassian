package org.apache.velocity.util;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FilesystemUtilsTest {
    // considering Windows naming convention
    // http://msdn.microsoft.com/en-us/library/windows/desktop/aa365247%28v=vs.85%29.aspx#naming_conventions
    @Test
    public void isSafePath_shouldAllowValidPathWithoutPathTraversal() {
        assertTrue(FilesystemUtils.isSafePath("a/valid/path/no/traversal/"));
        assertTrue(FilesystemUtils.isSafePath(Paths.get("a/valid/path/no/traversal/")));
        assertTrue(FilesystemUtils.isSafePath(Paths.get("a/valid/path/no/traversal")));
    }

    @Test
    public void isSafePath_shouldDisallowAbsolutePath() {
        assumeThat(System.getProperty("os.name").contains("win"), is(false));

        assertFalse(FilesystemUtils.isSafePath(Paths.get("/a/valid/absolute/path/")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("/a/valid/absolute/path")));
    }

    @Test
    public void isSafePath_shouldDisallowAbsolutePathWithTraversal() {
        assertFalse(FilesystemUtils.isSafePath(Paths.get("/a/valid/path/with/../traversal/")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("/a/valid/path/with/../traversal")));
    }

    @Test
    public void isSafePath_shouldDisallowPathWithPathTraversal() {
        assertFalse(FilesystemUtils.isSafePath(".."));
        assertFalse(FilesystemUtils.isSafePath("a/valid/path/with/../traversal/"));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a/valid/path/with/../traversal/")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a/valid/path/with/../traversal")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("../a/valid/path/with/traversal")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a/valid/path/with/traversal/..")));
    }

    @Test
    public void isSafePath_shouldAllowValidPathWithoutPathTraversalOnWindows() {
        assertTrue(FilesystemUtils.isSafePath(Paths.get("a\\valid\\path\\no\\traversal\\")));
        assertTrue(FilesystemUtils.isSafePath(Paths.get("a\\valid\\path\\no\\traversal")));
    }

    @Test
    public void isSafePath_shouldDisallowPathWithPathTraversalOnWindows() {
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a\\valid\\path\\with\\..\\traversal\\")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a\\valid\\path\\with\\..\\traversal")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("..\\a\\valid\\path\\with\\traversal\\")));
        assertFalse(FilesystemUtils.isSafePath(Paths.get("a\\valid\\path\\with\\traversal\\..")));
    }

    @Test
    public void isSafePath_doublePeriodDirectory() {
        assertTrue(FilesystemUtils.isSafePath(Paths.get("a/valid/path/with..out/traversal")));
    }

    @Test
    public void isSafePath_customImplAbsolutePath() {
        // Ensure #isAbsolute check is respected irrespective of other factors, to account for alternative filesystems
        Path altPathImpl = mock(Path.class);
        when(altPathImpl.isAbsolute()).thenReturn(true);
        when(altPathImpl.toString()).thenReturn("looks/like/non/absolute/path");

        assertFalse(FilesystemUtils.isSafePath(altPathImpl));
    }

    @Test
    public void isSafePath_customImplAbsolutePath2() {
        // Ensure #toString check is respected irrespective of other factors, to account for alternative Path implementations
        Path altPathImpl = mock(Path.class);
        when(altPathImpl.isAbsolute()).thenReturn(false);
        when(altPathImpl.toString()).thenReturn("/looks/like/absolute/path");

        assertFalse(FilesystemUtils.isSafePath(altPathImpl));
    }

    @Test
    public void containsPathTraversal() {
        assertTrue(FilesystemUtils.containsPathTraversal(".."));
        assertTrue(FilesystemUtils.containsPathTraversal("a/../b"));
        assertTrue(FilesystemUtils.containsPathTraversal("../b"));
        assertTrue(FilesystemUtils.containsPathTraversal("a/.."));
        assertFalse(FilesystemUtils.containsPathTraversal("a/b"));
    }

    @Test
    public void containsPathTraversal_win() {
        assertTrue(FilesystemUtils.containsPathTraversal("a\\..\\b"));
        assertTrue(FilesystemUtils.containsPathTraversal("..\\b"));
        assertTrue(FilesystemUtils.containsPathTraversal("a\\.."));
    }
}
