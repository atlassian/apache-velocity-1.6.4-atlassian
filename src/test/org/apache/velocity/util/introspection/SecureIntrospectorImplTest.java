package org.apache.velocity.util.introspection;

import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.Log;
import org.apache.velocity.util.SimplePoolTestCase;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Answers.RETURNS_DEEP_STUBS;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SecureIntrospectorImplTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock(answer = RETURNS_DEEP_STUBS)
    private RuntimeServices runtimeServices;

    @Mock
    private Log log;

    private SecureIntrospectorImpl secureIntrospector;

    // Class must be public for introspection
    public static class DummyObject implements Serializable {
        public void method(String param1, String param2) {
        }
        public void method2(String param1) {
        }
        public void method2() {
        }
    }

    public static class DummyObjectSubClass extends DummyObject {
    }

    private final DummyObject dummyInstance = new DummyObject();
    private final DummyObjectSubClass dummySubClassInstance = new DummyObjectSubClass();
    private final Method dummyMethod = DummyObject.class.getMethod("method", String.class, String.class);
    private final Method dummySubClassMethod = DummyObjectSubClass.class.getMethod("method", String.class, String.class);
    private final Method dummyMethod2 = DummyObject.class.getMethod("method2", String.class);
    private final Method dummySubClassMethod2 = DummyObjectSubClass.class.getMethod("method2", String.class);
    private final Method dummyMethod2NoParams = DummyObject.class.getMethod("method2");

    public SecureIntrospectorImplTest() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        when(runtimeServices.getConfiguration().getString(eq(RuntimeConstants.INTROSPECTOR_CACHE_CLASS), anyString()))
                .thenReturn(IntrospectorCacheImpl.class.getName());

        when(runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATION_SILENT, false)).thenReturn(false);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(false);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES)).thenReturn(new String[]{});
        when(runtimeServices.getConfiguration().getString(eq(RuntimeConstants.INTROSPECTOR_METHOD_TRANSLATOR), anyString()))
                .thenReturn(MethodTranslatorImpl.class.getName());
        refreshSecureIntrospector();
    }

    private void refreshSecureIntrospector() {
        secureIntrospector = new SecureIntrospectorImpl(log, runtimeServices);
    }

    @Test
    public void parameterFiltering() {
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{"regular", "regular"}));
        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{"regular", "../traversal"}));
    }

    @Test
    public void parameterFiltering_exemptions() {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_METHODS)).thenReturn(new String[]{"method2"});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{"regular", "../traversal"}));
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod2, new Object[]{"regular", "../traversal"}));
    }

    @Test
    public void topLevelRestrictions_classWaitNotify() throws Exception {
        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, DummyObject.class.getMethod("wait"), new Object[]{}));
        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, DummyObject.class.getMethod("notify"), new Object[]{}));
    }

    @Test
    public void topLevelExemptions_classGetName() throws Exception {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES)).thenReturn(new String[]{Class.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance.getClass(), Class.class.getMethod("toString"), new Object[]{}));
        assertTrue(secureIntrospector.isExecutionRestricted(null, Class.class.getMethod("toString"), new Object[]{}));

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance.getClass(), Class.class.getMethod("getName"), new Object[]{}));
        assertFalse(secureIntrospector.isExecutionRestricted(null, Class.class.getMethod("getName"), new Object[]{}));
    }

    @Test
    public void topLevelExemptions_numberBooleanString() throws Exception {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{Integer.class.getName(), Boolean.class.getName(), String.class.getName()});
        refreshSecureIntrospector();

        Integer integer = 42;
        assertFalse(secureIntrospector.isExecutionRestricted(integer, Integer.class.getMethod("floatValue"), new Object[]{}));
        assertFalse(secureIntrospector.isExecutionRestricted(null, Integer.class.getMethod("floatValue"), new Object[]{}));

        Boolean bool = true;
        assertFalse(secureIntrospector.isExecutionRestricted(bool, Boolean.class.getMethod("booleanValue"), new Object[]{}));
        assertFalse(secureIntrospector.isExecutionRestricted(null, Boolean.class.getMethod("booleanValue"), new Object[]{}));

        String str = "yes";
        assertFalse(secureIntrospector.isExecutionRestricted(str, String.class.getMethod("isEmpty"), new Object[]{}));
        assertFalse(secureIntrospector.isExecutionRestricted(null, String.class.getMethod("isEmpty"), new Object[]{}));
    }

    @Test
    public void restrictedClass() {
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));

        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedClass_inheritance() {
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));

        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{Serializable.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedClass_array() throws Exception {
        DummyObject[] dummyArray = new DummyObject[0];
        Method dummyMethod = DummyObject[].class.getMethod("hashCode");

        assertFalse(secureIntrospector.isExecutionRestricted(dummyArray, dummyMethod, new Object[]{}));

        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyArray, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedPackage() {
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));

        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedPackage_classExemption() {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedPackage_altClassExemption() {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void restrictedPackage_packageExemption() {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void deferredClassLoading() {
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});

        Supplier<Boolean> mockedIsReady = (Supplier<Boolean>) mock(Supplier.class);
        final boolean[] classesLoaded = {false};

        secureIntrospector = new SecureIntrospectorImpl(log, runtimeServices) {
            @Override
            protected boolean isIntrospectorEnabled() {
                return mockedIsReady.get();
            }

            @Override
            protected Class<?> loadClass(String name) throws ClassNotFoundException {
                classesLoaded[0] = true;
                return super.loadClass(name);
            }
        };

        when(mockedIsReady.get()).thenReturn(false);
        secureIntrospector.getMethod(dummyInstance, dummyInstance.getClass(), "method2", new Object[]{""});
        // When application is not ready, assert that classes aren't loaded
        assertFalse(classesLoaded[0]);

        when(mockedIsReady.get()).thenReturn(true);
        // Once application is ready, assert classes are loaded
        secureIntrospector.getMethod(dummyInstance, dummyInstance.getClass(), "method2", new Object[]{""});
        assertTrue(classesLoaded[0]);
    }

    @Test
    public void allowlistedClass() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));

        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void allowlistedMethod_toMethodStr() {
        assertEquals("method2(java.lang.String)", SecureIntrospectorImpl.toMethodStr(dummyMethod2));
        assertEquals("method(java.lang.String java.lang.String)", SecureIntrospectorImpl.toMethodStr(dummyMethod));
        assertEquals("method2()", SecureIntrospectorImpl.toMethodStr(dummyMethod2NoParams));
    }

    @Test
    public void allowlistedMethod_toMethodSet() {
        Map<Class<?>, Set<String>> result = secureIntrospector.toMethodSet(new String[]{
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2(java.lang.String)"
        });
        assertThat(result).hasSize(1).containsKey(DummyObject.class)
                .containsValue(new HashSet<>(singletonList("method2(java.lang.String)")));

        result = secureIntrospector.toMethodSet(new String[]{
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2()",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method(java.lang.String java.lang.String)"
        });
        assertThat(result).hasSize(1).containsKey(DummyObject.class)
                .containsValue(new HashSet<>(asList("method2(java.lang.String)", "method2()", "method(java.lang.String java.lang.String)")));
    }

    @Test
    public void allowlistedMethod_toMethodSet_skipsInvalidClass() {
        Map<Class<?>, Set<String>> result = secureIntrospector.toMethodSet(new String[]{
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2(java.lang.String)",
                "org.apache.velocity.util.introspection.NonExistentClass#method2(java.lang.String)"
        });
        assertThat(result).hasSize(1).containsKey(DummyObject.class)
                .containsValue(new HashSet<>(singletonList("method2(java.lang.String)")));
    }

    @Test
    public void allowlistedMethod_toMethodSet_skipsInvalidMethods() {
        Map<Class<?>, Set<String>> result = secureIntrospector.toMethodSet(new String[]{
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method#invalid#(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#nonExistentMethod()"
        });
        assertThat(result).hasSize(1).containsKey(DummyObject.class)
                .containsValue(new HashSet<>(singletonList("method2(java.lang.String)")));
    }

    @Test
    public void allowlistedMethod_toMethodSet_silentValidation() {
        // Configuration contains 4 invalid entries but 2 are common due to same invalid class
        String[] methodsParam = new String[]{
                "org.apache.velocity.util.introspection.NonExistentClass#method3(java.lang.String)",
                "org.apache.velocity.util.introspection.NonExistentClass#method2(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method2(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#method#invalid#(java.lang.String)",
                "org.apache.velocity.util.introspection.SecureIntrospectorImplTest$DummyObject#nonExistentMethod()"
        };
        int expectedValidationErrors = 3;

        // Warn logging expected in default configuration
        secureIntrospector.toMethodSet(methodsParam);
        verify(log, times(expectedValidationErrors)).warn(anyString());

        // Silent validation enabled
        when(runtimeServices.getConfiguration().getBoolean(RuntimeConstants.INTROSPECTOR_VALIDATION_SILENT, false)).thenReturn(true);
        refreshSecureIntrospector();
        reset(log);

        // Debug logging expected
        secureIntrospector.toMethodSet(methodsParam);
        verify(log, times(expectedValidationErrors)).debug(anyString());
        verify(log, never()).warn(anyString());
    }

    @Test
    public void allowlistedMethod() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS))
                .thenReturn(new String[]{DummyObject.class.getName() + "#method2(java.lang.String)"});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod2NoParams, new Object[]{}));
        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod2, new Object[]{}));
    }

    /**
     * The method allowlisting mechanism was intentionally designed to be based on the declaring class of a method. That
     * is, the Introspector does not require (and will not acknowledge) any allowlist entries for a method on a class
     * that is inherited. Allowlisting a method based on the declaring class doesn't guarantee that method's behaviour
     * will be identical in a subclass, but it's an acknowledged mild risk that enables the allowlist configuration to
     * be much shorter by not needing to allowlist every subclass where the method declaration hasn't changed.
     */
    @Test
    public void allowlistedMethod_declaringClassBased() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_METHODS))
                .thenReturn(new String[]{
                        DummyObjectSubClass.class.getName() + "#method2(java.lang.String)", // Inconsequential as inherited
                        DummyObject.class.getName() + "#method(java.lang.String java.lang.String)"});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummySubClassInstance, dummySubClassMethod, new Object[]{}));
        assertTrue(secureIntrospector.isExecutionRestricted(dummySubClassInstance, dummySubClassMethod2, new Object[]{}));
    }

    @Test
    public void allowlistedPackage() throws Exception {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
        assertTrue(secureIntrospector.isExecutionRestricted(
                new SimplePoolTestCase(""), SimplePoolTestCase.class.getMethod("run"), new Object[]{}));
    }

    @Test
    public void allowlistedPackage_restrictionsPrecedence() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_PACKAGES))
                .thenReturn(new String[]{DummyObject.class.getPackage().getName()});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_RESTRICT_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void allowlist_restrictedPackageExemptionsIndependent() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOW_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        when(runtimeServices.getConfiguration().getStringArray(RuntimeConstants.INTROSPECTOR_ALLOWLIST_CLASSES))
                .thenReturn(new String[]{DummyObject.class.getName()});
        refreshSecureIntrospector();

        assertTrue(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }

    @Test
    public void allowlistDebugMode_shouldAllowNonAllowlistedMethods() {
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_ENABLE, false)).thenReturn(true);
        when(runtimeServices.getConfiguration()
                .getBoolean(RuntimeConstants.INTROSPECTOR_PROPER_ALLOWLIST_DEBUG, false)).thenReturn(true);
        refreshSecureIntrospector();

        assertFalse(secureIntrospector.isExecutionRestricted(dummyInstance, dummyMethod, new Object[]{}));
    }
}
