package org.apache.velocity.util.introspection;

import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import static org.junit.Assert.assertEquals;

public class MethodTranslatorImplTest {

    private final MethodTranslatorImpl methodTranslatorImpl = new MethodTranslatorImpl();

    @Test
    public void getTranslatedMethod_proxiedInstance() throws Exception {
        DummyInterface originalObject = new DummyObject();
        DummyInterface proxyInstance = newDynamicProxy(originalObject, DummyInterface.class);

        Method interfaceMethod = DummyInterface.class.getMethod("method");
        assertEquals(interfaceMethod, methodTranslatorImpl.getTranslatedMethod(proxyInstance, proxyInstance.getClass().getMethod("method")));
    }

    @Test
    public void getTranslatedMethod_regularInstance() throws Exception {
        DummyInterface originalObject = new DummyObject();
        Method instanceMethod = DummyObject.class.getMethod("method");
        assertEquals(instanceMethod, methodTranslatorImpl.getTranslatedMethod(originalObject, instanceMethod));
    }

    @SuppressWarnings("unchecked")
    public static <T> T newDynamicProxy(T originalObject, Class<T> proxyInterface) {
        return (T) Proxy.newProxyInstance(
                proxyInterface.getClassLoader(),
                new Class<?>[] { proxyInterface },
                new SimpleInvocationHandler(originalObject)
        );
    }

    private static final class DummyObject implements DummyInterface {
        @Override
        public void method() {
        }
    }

    private interface DummyInterface {
        void method();
    }

    public static class SimpleInvocationHandler implements InvocationHandler {
        private final Object instance;

        public SimpleInvocationHandler(Object instance) {
            this.instance = instance;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            return method.invoke(instance, args);
        }
    }
}
