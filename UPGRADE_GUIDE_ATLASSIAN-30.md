# 1.6.4-atlassian-30 Upgrade guide

## For host products (e.g. Confluence)

### File allowlist

We've added an allowlist for files to prevent the Velocity engine from rendering arbitrary files from the filesystem. This makes it slightly harder to escalate privileges for a threat actor.

To adopt:
1. Find all Velocity files to be allowlisted. We just need to gather the relative file paths for the allowlist, the file paths should be relative to the unpacked distribution of the product. An example:
   1. Build the product standalone distribution (maybe even fetch it from CI)
   2. Open the directory of your terminal. Hint: You should see the `WEB-INF` and `META-INF` folders
   3. Run `fdfind ".(vm|vmd|vtl)$" > velocity-allowlisted-files.txt`
2. Bump the version of this dependency
3. Make the same changes to relevant `ResourceLoader` implementations that exist in your product using `ClasspathResourceLoader` as a reference. Known product implementations to change: Confluence `DynamicPluginResourceLoader`, Jira `PluginVelocityResourceLoader`, and Bamboo `FileResourceLoader`.
4. Run ALL your tests in CI to make sure the allowlist is complete
5. Add a functional test to your product that a Velocity file added to the webapp distribution cannot be rendered by the engine
6. Add a test that the allowlist does not contain more than is necessary, i.e. it ratchets down

### Filetype allowlist

We've added an allowlist based on file extensions to prevent the Velocity engine from deciding to render any old file. In case it's not clear enough, Velocity previously didn't care what the extension was, it would try to render it as a template file. By adding an allowlist we make it slightly harder to escalate privileges for a threat actor.

This allowlist is automatically bypassed by default on OSGi bundles (infer: P2 plugins) and JAR files as these are no easier to replace than the `velocity.properties` file itself.

To adopt:
1. Bump the version of this dependency
2. Update your `velocity.properties` file with `file.resource.loader.filetype.allowlist` if it needs to differ from the default. This allowlist is for the `FileResourceLoader` and should only ever be used for loading files from the webapp folder 
3. Update your `velocity.properties` file with `classpath.resource.loader.filetype.allowlist` if it needs to differ from the default. This allowlist is for the `ClasspathResourceLoader` this will be used for files in the webapp folder, JARs (e.g. product core), and plugins
4. Make the same changes to relevant `ResourceLoader` implementations that exist in your product using `ClasspathResourceLoader` as a reference. Known product implementations to change: Confluence `DynamicPluginResourceLoader`, Jira `PluginVelocityResourceLoader`, and Bamboo `FileResourceLoader`
5. Run ALL your tests in CI to make sure the allowlist is complete
6. Add a functional test to your product that a Velocity template with an invalid file extension in the webapp distribution cannot be rendered by the engine

## For Confluence specifically

Remove the existing filetype allowlist in `ConfigurableResourceManager`
