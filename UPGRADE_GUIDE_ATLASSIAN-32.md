# 1.6.4-atlassian-32 Upgrade guide

## For host products (e.g. Confluence)

### Method/class/package allowlists

This version introduces an optional method/class/package allowlist in addition to the existing exclusion list options.

#### Getting Started

To enable it, please first set `introspector.proper.allowlist.enable`. Then you must specify all the methods which you
wish to permit invocation. This is done by setting `introspector.proper.allowlist.methods` to a comma-separated list of
methods, which take the form `package.Class#method(paramType1 paramType2)`. For example:

```
introspector.proper.allowlist.enable = true

introspector.proper.allowlist.methods = com.atlassian.confluence.util.HtmlUtil#urlEncode(java.lang.String),\
com.atlassian.confluence.util.i18n.DefaultI18NBean#getText(com.atlassian.confluence.util.i18n.Message),\
com.atlassian.confluence.util.i18n.DefaultI18NBean#getText(java.lang.String [Ljava.lang.Object;),\
com.atlassian.confluence.util.i18n.DefaultI18NBean#getText(java.lang.String java.util.List),\
com.atlassian.confluence.util.i18n.DefaultI18NBean#getText(java.lang.String),\
com.atlassian.confluence.util.i18n.DefaultI18NBean#getTextStrict(java.lang.String),\
com.atlassian.confluence.util.i18n.Message#getArguments(),\
com.atlassian.confluence.util.i18n.Message#getKey()
```

The following options also exist but it is recommended to avoid them if possible as they are inherently less secure:

* `introspector.proper.allowlist.classes`
* `introspector.proper.allowlist.packages`

#### Allowlisting Proxied Instances

The allowlist is understandably ineffective when dealing with dynamic proxies whose underlying class is unknown. To
circumvent this, the configuration option `introspector.method.translator` can be set to a class which implements the
`org.apache.velocity.util.introspection.MethodTranslator` interface. A default implementation is provided which simply
finds the first matching interface. A more sophisticated implementation will provide greater security. A custom Spring
and Hibernate proxy aware implementation (`AtlassianMethodTranslator`) can be found in the `velocity-allowlist`
module in the Atlassian Template Renderer repository. We recommend bundling this artefact with your product and
configuring it, if you use Spring or Hibernate proxies.

#### Further Customisation

Please note, as per all previous template invocation security features, these capabilities are only available when you
have configured `runtime.introspector.uberspect` to `org.apache.velocity.util.introspection.SecureUberspector` or a
class which extends it.

`SecureUberspector` initialises the `org.apache.velocity.util.introspection.SecureIntrospectorImpl` class which is what
enforces the allowlist and other template invocation security features.

By overriding `SecureUberspector#init` you can provide a custom introspector implementation which extends the default
`SecureIntrospectorImpl`.

The following overridable methods in `SecureIntrospectorImpl` are particularly useful in integrating the allowlist:

* `#loadClass` - allows specifying a custom class loader or loading mechanism for the exclusion/allowlists
* `#isIntrospectorEnabled` - by returning `false` on startup, you can defer loading the classes specified by the 
  exclusion/allowlists until a later point in time, thereafter the introspector can also be toggled at will
* `#isAllowlistedClassPackageInternal` - modify the class/package allowlist criteria with your own dynamic configuration
  (be wary of caching implications)
* `#isAllowlistedMethodInternal` - same as previous, but for methods

Server product suitable subclasses (`PluginAwareSecureIntrospector` and `PluginAwareSecureUberspector`) which leverage
the above capabilities can be found, alongside further details, in the `velocity-allowlist` module in the Atlassian
Template Renderer repository.

#### Optimisations

An additional note for applications which utilise the Atlassian Template Renderer:

An `Uberspector` and accompanying `Introspector` are constructed anew for each new Velocity engine. Since, the Atlassian
Template Renderer creates new engines as needed, the initialisation cost of the `SecureIntrospectorImpl` is multiplied.
This can be worked around by configuring a delegating, singleton-enforcing `Uberspector`. An implementation
(`SingletonUberspector`) is provided in the `velocity-allowlist` module in the Atlassian Template Renderer repository.
The aforementioned `PluginAwareSecureIntrospector` already extends this class.
